#!/bin/bash
#
# agent - Switches between SSH agents
#
# Usage:
#   agent list              - list the available agents
#   agent which             - show the default agent
#   agent choose            - choose a default agent (interactive)
#   agent set [socket-path] - switch to specific agent
#   agent fix               - attempt to fix a zombie default agent

DEFAULT_AGENT="$HOME/.ssh/default-agent"
LOCAL_AGENT="/tmp/ssh-agent-$USER"

main() {
  local cmd="$1"
  shift

  case "$cmd" in
    list   | l) list-agents ;;
    which  | w) default-agent;;
    choose | c) choose-agent;;
    set    | s) set-agent "$@";;
    fix    | f) fix;;
    *)          usage "$0";;
  esac
}

list-agents() {
  echo 'local'

  if [ -n "$(find /tmp/ssh-* -iname 'agent*')" ]; then
    stat -c "%n (%y)" /tmp/ssh-*/agent*
  fi
}

default-agent() {
  if [ -e $DEFAULT_AGENT ]; then
    local real="$(readlink $DEFAULT_AGENT)"
    if [ -e "$real" ]; then
      echo $real
    fi
  fi
}

choose-agent() {
  local agent=$(list-agents | fzf | cut -d' ' -f1)
  if [ -z "$agent" ]; then
    error 'User aborted'
    exit 1
  fi
  set-agent "$agent"
}

set-agent() {
  local agent="$1"
  if [ "$agent" == 'local' ]; then
    local agent="$LOCAL_AGENT"
  fi
  if ! is-agent-running "$agent"; then
    ssh-agent -a "$agent" > /dev/null
  fi

  echo "Setting default agent: $agent"
  ln -sf $agent $DEFAULT_AGENT
}

fix() {
  echo
  if ! is-agent-running "$DEFAULT_AGENT"; then
    set-agent 'local'
  fi
}

is-agent-running() {
  [ -n "$1" -a -e "$1" ]
}

main "$@"
