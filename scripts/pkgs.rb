#!/usr/bin/env ruby

require "#{ENV['DOTF']}/framework.rb"
require 'date'

require_relative './packages/reader'
require_relative './packages/status'
require_relative './packages/tracker'
require_relative './packages/export'

def main
  if ARGV.length == 0
    usage
  else
    cmd = ARGV.shift.to_sym
    packages = PackageReader.read('~/.packages')
    case cmd
    when :status
      PackageStatus.print(packages)
    when :track
      PackageTracker.track(packages)
    when :export
      PackageExport.export(packages)
    end
  end
end

def usage
  puts <<-EOF
pkgs - package tracking utility

Usage:

  pkgs status
  pkgs show

  EOF
end

main
