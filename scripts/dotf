#!/bin/bash
#
# Elentok's dotfiles
#
# Usage:
#
#   dotf list                         - lists all plugins
#   dotf install                      - installs all plugins
#   dotf install [plugin] [plugin]... - install specific plugins
#   dotf update                       - pulls from git and reruns install
#   dotf export-vim                   - exports vim config to .tar.gz

source `dirname $0`/../framework

benchmark-start

main() {
  cmd="$1"
  shift
  case "$cmd" in
  list | l)
    list_plugins
    ;;
  install | i)
    install "$@"
    ;;
  update | u)
    update
    ;;
  export-vim)
    export_vim
    ;;
  *)
    usage "$0"
    ;;
  esac
}

list_plugins() {
  (
    echo vim
    echo zsh
    /bin/ls -1 $DOTF/plugins
    /bin/ls -1 $DOTL/plugins 2>/dev/null
  ) | sort
}

install() {
  (cd $DOTF && git submodule update --init --recursive)

  if [ $# -gt 0 ]; then
    plugins "$@"
    return
  fi

  is_workstation && install_workstation
  is_server && install_server
  show_done
}

install_workstation() {
  common_plugins
  is_mac && mac_plugins
  is_linux && linux_plugins
}

install_server() {
  plugin zsh
  plugin vim
}

update() {
  header "Updating dotfiles"
  cd $DOTF

  bullet "Pulling changes... "
  old_head=`git rev-parse HEAD`
  git pull
  new_head=`git rev-parse HEAD`

  if [ "$old_head" != "$new_head" ]; then
    echo -e "\nPulled the following changes:"
    git changelog ${old_head}..${new_head}

    echo -ne "\nPress any key to run the installer..."
    read -n 1

    if is_mac; then
      bullet 'Updating brew... '
      brew update
    fi

    install
  fi
}

show_done() {
  echo -e "$GREEN=========================="
  echo "Done ($(benchmark-stop))"
  echo -e "==========================$RESET"
}

common_plugins() {
  plugin nodejs
  plugin git
  plugin zsh
  plugin bash
  plugin tmux
  plugin vim
  plugin ruby
  # plugin mpd
}

mac_plugins() {
  # plugin homebrew-cask
  # plugin karabiner
  plugin osx-tuning
  plugin iterm
  brew_install imagemagick
  brew_install ghostscript
  brew_install ncdu
  # brew_cask_install spectacle
}

linux_plugins() {
  if is_arch; then
    pacman_install htop
  else
    apt_install htop
  fi

  if [ "$HAS_GUI" = 'yes' ]; then
    plugin linux-gui-apps
  fi
}

plugins() {
  if [ $# -eq 0 ]; then
    error "No plugins specified"
    return
  fi

  while [ $# -gt 0 ]; do
    plugin $1
    shift
  done
  show_done
}

plugin() {
  local filename="$(find_plugin "$1")"

  if [ -z "$filename" ]; then
    error "Cannot find plugin '$1'"
    return
  fi

  $filename
}

find_plugin() {
  local filenames=(
    "$DOTF/$1/install"
    "$DOTF/plugins/$1/install"
    "$DOTF/plugins/$1"
    "$DOTL/plugins/$1/install"
    "$DOTL/plugins/$1"
  )

  for filename in ${filenames[@]}; do
    if [ -x "$filename" ]; then
      echo "$filename"
      return 0
    fi
  done

  return 1
}

export_vim() {
  tar czvf vim-$(date +%Y%m%d).tar.gz \
    --exclude 'YouCompleteMe' \
    --exclude 'node_modules' \
    --exclude '.git' \
    vim framework
}

main "$@"
