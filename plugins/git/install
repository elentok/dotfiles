#!/bin/bash

source `dirname $0`/../../framework

GITCONFIG="$HOME/.gitconfig"

main() {
  header "Git"
  if ! has_command git; then
    install_git
  fi
  install_tig
  install_gitconfig
  install_symlinks
  install_diff_so_fancy
}

install_git() {
  if is_mac; then
    brew_install git
  elif is_arch; then
    pacman_install git
  else
    apt_install git libncurses5-dev
  fi
}

install_tig() {
  if is_arch; then
    pacman_install tig
  else
    brew_install tig
  fi
}

install_gitconfig() {
  bullet "Installing $GITCONFIG... "
  if [ -e $GITCONFIG -o -h $GITCONFIG ]; then
    fix_existing_gitconfig
  else
    write_to_gitconfig
    success 'done'
  fi
}

fix_existing_gitconfig() {
  if [ -h $GITCONFIG ]; then
    backup $GITCONFIG
    write_to_gitconfig
    success 'done'
  else
    if [ -n "$(grep "path = $DOTF/plugins/git/gitconfig" $GITCONFIG)" ]; then
      info 'already installed'
    else
      echo >> $GITCONFIG
      write_to_gitconfig
      success 'done'
    fi
  fi
}

write_to_gitconfig() {
  if [ ! -e $GITCONFIG ]; then
    touch $GITCONFIG
  fi
  echo "[include]" >> $GITCONFIG
  echo "  path = $DOTF/plugins/git/gitconfig" >> $GITCONFIG
}

install_symlinks() {
  symlink "$DOTF/plugins/git/tigrc" ~/.tigrc
}

install_diff_so_fancy() {
  if is_mac; then
    brew_install diff-so-fancy
  elif is_arch; then
    pacman_install diff-so-fancy
  else
    npm_install diff-so-fancy
  fi
}

main "$@"
